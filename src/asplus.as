var qual = {
    best: function()
    {
        _root._quality = "BEST";
    },
    high: function()
    {
        _root._quality = "HIGH";
    },
    med: function()
    {
        _root._quality = "MEDIUM";
    },
    medium: function()
    {
        _root._quality = "MEDIUM";
    },
    low: function()
    {
        _root._quality = "LOW";
    },
    setQualKeys: function()
    {
        if (Key.isDown(66)) {
            // B or b key
            _root._quality = "BEST";
        } 
        else if (Key.isDown(72)) {
            // H or h key
            _root._quality = "HIGH";
        } 
        else if (Key.isDown(77)) {
            // M or m key
            _root._quality = "MEDIUM";
        } 
        else if (Key.isDown(76)) {
            // L or l key
            _root._quality = "LOW";
        } 
        else if (Key.isDown(65)) {
            // A or a key
            _root._quality = "AUTO";
        } 
        else {
            _root._quality = "AUTO";   
        }
    }
}

function playFrame(pframe)
{
    gotoAndPlay(pframe);
}

function stopFrame(sframe)
{
    gotoAndStop(sframe);
}

function goFrame(gframe, type)
{
    if (type == "stop" || type == 1) {
        gotoAndStop(gframe);
    }
    else if (type == "play" || type == 2) {
        gotoAndPlay(gframe);
    }
    else {
        gotoAndStop(gframe);
    }
}